document.addEventListener("DOMContentLoaded", function(event) { 
});

var slider = tns({
    container: '#gallery',
    items: 1,
    slideBy: 'page',
    autoHeight: false,
    controlsText: ['<div class="prev"></div>','<div class="next"></div>'],
    controlsPosition: "bottom",
    mouseDrag: true,
    speed: 800,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 6000,
    animateIn: "fadeIn",
    animateOut: "fadeOut"
});


// If form field has no value then check

var $input = document.getElementsByClassName("form-field");

for (i = 0; i < $input.length; i++) {
    var thisInput = $input[i];
    thisInput.addEventListener("focusout", function(thisInput) {        
        if(thisInput.srcElement.value === "") {
            thisInput.srcElement.classList.remove("active");
        } else {
            thisInput.srcElement.classList.add("active");
        }
    });
}


$(function() {

    // Get the form.
    var form = $('#ajax-contact');

    // Get the messages div.
    var formMessages = $('#form-messages');

    // Set up an event listener for the contact form.
    $(form).submit(function(e) {
        // Stop the browser from submitting the form.
        e.preventDefault();
        $(this).find('button').addClass("loading");

        // Serialize the form data.
        var formData = $(form).serialize();
        
        // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        })
        .done(function(response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');

            // Set the message text.
            $(formMessages).children().text(response);

            // Clear the form.
            $('#fname').val('');
            $('#lname').val('');
            $('#email').val('');
            $('#phone').val('');
            $('#address').val('');
            
            $(this).find('button').removeClass("loading");

        })
        .fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');

            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).children().text(data.responseText);
            } else {
                $(formMessages).children().text('Oops! An error occured and your message could not be sent.');
            }
            
            $(this).find('button').removeClass("loading");
        });

    });

});

